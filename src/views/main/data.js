import { nav1, nav2, nav3, nav4, nav5, nav6, nav7, nav1ac, nav2ac, nav3ac, nav4ac, nav5ac, nav6ac, nav7ac } from './menupicture'
export const menuList = [
    {
        title: '通讯录',
        path: '/main/address', 
        icon: nav1,
        activeIcon: nav1ac
    },
    // {
    //     title: '搜索',
    //     path: '/main/search',
    //     icon: nav1,
    //     activeIcon: nav1ac,
    //     isShow: '0'
    // },
    {
        title: '快速拨号',
        path: '/main/dial',
        icon: nav2,
        activeIcon: nav2ac
    },
    {
        title: '会议',
        path: '/main/meet',
        icon: nav3,
        activeIcon: nav3ac
    },
    // {
    //     title: '发起会议',
    //     path: '/main/meet/launchmeet',
    //     icon: nav3,
    //     activeIcon: nav3ac
    // },
    {
        title: '视频监控',
        path: '/main/monitor',
        icon: nav4,
        activeIcon: nav4ac
    },
    {
        title: '集群对讲',
        path: '/main/intercom',
        icon: nav5,
        activeIcon: nav5ac
    },
    {
        title: '地图',
        path: '/main/map',
        icon: nav6,
        activeIcon: nav6ac
    },
    {
        title: '我的',
        path: '/main/my',
        icon: nav7,
        activeIcon: nav7ac
    },
]