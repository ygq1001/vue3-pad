import microphone from '@/assets/call/microphone.png'
import camera from '@/assets/call/camera.png'
import horn from '@/assets/call/horn.png'

import redmicrophone from '@/assets/call/redmicrophone.png'
import redcamera from '@/assets/call/redcamera.png'
import redhorn from '@/assets/call/redhorn.png'




import sharing from '@/assets/call/sharing.png'
import record from '@/assets/call/record.png'
import member from '@/assets/call/member.png'
import invite from '@/assets/call/invite.png'


import redrecord from '@/assets/call/redrecord.png'



export {
    microphone,
    camera,
    horn,
    redmicrophone,
    redcamera,
    redhorn,
    sharing,
    record,
    member,
    invite,
    redrecord


}