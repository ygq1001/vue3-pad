import request from './index'
// import { get } from './index'
// 获取树
export const getTree = (data: any) => {
    return request.get("/authenm/sysOrg/tree", {
        params: data
    });
}

//获取人员
export const getUser = (data: any) => {
    return request.get("/authenm/sysUser/list", { 
        params: data
    });
}


// export const getTree = async (payload = {}) => {
//     const params = {
//         url: '/authenm/sysOrg/tree',
//         payload,
//     }
//     const result = await get(params)
//     if (result && result.code == 200) {
//         console.log("成功");
        
//         return result
//     }
//     return false
// }