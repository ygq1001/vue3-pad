import axios from "axios";
import { message } from 'ant-design-vue';
// import { useUserStore } from '@/store/modules/user'


let request = axios.create({
    // baseURL: "http://192.168.10.60:8099",    
    baseURL: "",

    // baseURL: "http://43.143.123.115:8099",
    timeout: 5000
})
// eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXNzd2QiOiIyMDdjZjQxMDUzMmY5MmE0N2RlZTI0NWNlOWIxMWZmNzFmNTc4ZWJkNzYzZWIzYmJlYTQ0ZWJkMDQzZDAxOGZiIiwidXNlcnR5cGUiOiIxIiwiaWQiOiIxNzc1MDQ5NTg2MTUxMzAxMTIxIiwiaXNBZG1pbiI6IjAiLCJleHAiOjE3MTY5ODYzMzcsImlhdCI6MTcxNjk0MzEzNywiYWNjb3VudCI6Imd5MyJ9.FzD-zB_FHDLdWN5Ft2WuHX8-Uq8ns3xO7sg5K4KZuTk

const Token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwYXNzd2QiOiIyMDdjZjQxMDUzMmY5MmE0N2RlZTI0NWNlOWIxMWZmNzFmNTc4ZWJkNzYzZWIzYmJlYTQ0ZWJkMDQzZDAxOGZiIiwidXNlcnR5cGUiOiIxIiwiaWQiOiIxNzc1MDQ5NTg2MTUxMzAxMTIxIiwiaXNBZG1pbiI6IjAiLCJleHAiOjE3MTcwNzI3NjksImlhdCI6MTcxNzAyOTU2OSwiYWNjb3VudCI6Imd5MyJ9.95BmuWyQjx68wvBl_nT_V0EH9k7R9Im0DYUM14nmWuQ'
//请求拦截器
request.interceptors.request.use(config => {
    // const useStore=useUserStore();
    // if(useStore.token){
    // config.headers.Token = useStore.token
    // }

    Object.assign(config.headers, { Token: Token, Tenantid: 'web' })
    return config;
});
//响应拦截器
request.interceptors.response.use((response) => {
    //成功回调
    return response.data;//简化数据  
}, (error) => {
    //处理网络错误
    let msg = '';
    //http状态码
    let status = error.response.status;
    switch (status) {
        case 401:
            msg = "token过期";
            break;
        case 403:
            msg = '无权访问';
            break;
        case 404:
            msg = "请求地址错误";
            break;
        case 500:
            msg = "服务器出现问题";
            break;
        default:
            msg = "网络出现问题";

    }
    // ElMessage({
    //     type: 'error',
    //     message: msg
    // })
    message.error(msg)
    return Promise.reject(error);
});



// export function get(params) {
//     const { url, payload } = params
//     return new Promise((resolve, reject) => {
//         request.get(url, { params: payload })
//             .then(response => {
//                 resolve(response.data);
//             })
//             .catch(err => {
//                 reject(err)
//             })
//     })
// }

export default request;