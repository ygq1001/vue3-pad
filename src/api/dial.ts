import request from './index'

//通话记录1
export const getRecord = (data: any) => {
    data.cmd = 'callDetail'
    return request.get("/dispatch/api", {
        params: data
    });
}