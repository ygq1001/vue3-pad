import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/main',
      name: 'main',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/main/main.vue'),
      children: [
        {
          path: '/',
          redirect: '/main/address'
        },
        {
          path: '/main/address',
          name: 'address',
          component: () => import('../views/address/address.vue'),
          meta: {
            keepAlive: true, // 设置需要缓存的组件
          },
        },
        {
          path: '/main/search',
          name: 'search',
          component: () => import('../views/search/search.vue'),
          meta: {
            show: '0'
          }

        },
        {
          path: '/main/dial',
          name: 'dial',
          component: () => import('../views/dial/dial.vue'),
        },
        //会议
        {
          path: '/main/meet',
          name: 'meet',
          component: () => import('../views/meet/meet.vue'),
        },
        {
          path: '/main/monitor',
          name: 'monitor',
          component: () => import('../views/monitor/monitor.vue'),
        },
        {
          path: '/main/intercom',
          name: 'intercom',
          component: () => import('../views/intercom/intercom.vue'),
        },
        {
          path: '/main/map',
          name: 'map',
          component: () => import('../views/map/map.vue'),
        },
        {
          path: '/main/my',
          name: 'my',
          component: () => import('../views/my/my.vue'),
        },
      ]
    },
    {
      path: '/call',
      name: 'call',
      component: () => import('../views/call/call.vue'),
    },
    //发起会议
    {
      path: '/launchmeet',
      name: 'launchmeet',
      component: () => import('../views/meet/launchmeet/launchmeet.vue'),
    },
    //邀请
    {
      path: '/invite',
      name: 'invite',
      component: () => import('../views/invite/invite.vue'),
      meta: {
        keepAlive: true, // 设置需要缓存的组件
      },
    },
    //搜索邀请
    {
      path: '/isearch',
      name: 'isearch',
      component: () => import('../views/isearch/isearch.vue'),
    }
  ]


})

export default router
