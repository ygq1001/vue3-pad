import './assets/main.css'
import './assets/style/font.scss'
import './styles/index'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css';
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersist from 'pinia-plugin-persist' 
import App from './App.vue'
import router from './router'
const pinia = createPinia()
// import 'normal.css'

const app = createApp(App)
pinia.use(piniaPluginPersist)
app.use(pinia)
app.use(router)
app.use(Antd) 
app.mount('#app')



