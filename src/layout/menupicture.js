import nav1 from '@/assets/nav/nav1.png'
import nav2 from '@/assets/nav/nav2.png'
import nav3 from '@/assets/nav/nav3.png'
import nav4 from '@/assets/nav/nav4.png'
import nav5 from '@/assets/nav/nav5.png'
import nav6 from '@/assets/nav/nav6.png'
import nav7 from '@/assets/nav/nav7.png'
import nav1ac from '@/assets/nav/nav1ac.png'
import nav2ac from '@/assets/nav/nav2ac.png'
import nav3ac from '@/assets/nav/nav3ac.png'
import nav4ac from '@/assets/nav/nav4ac.png'
import nav5ac from '@/assets/nav/nav5ac.png'
import nav6ac from '@/assets/nav/nav6ac.png'
import nav7ac from '@/assets/nav/nav7ac.png'


export {
    nav1,
    nav2,
    nav3,
    nav4,
    nav5,
    nav6,
    nav7,
    nav1ac,
    nav2ac,
    nav3ac,
    nav4ac,
    nav5ac,
    nav6ac,
    nav7ac,
}

