import { defineStore } from 'pinia'
// 定义状态的类型

export const useStore = defineStore('main', {
  state: () => {
    return {
      bread: [],
      historyRecord: [],
      userCardState: 'show',
      hasSelect:[],
      visible:false,
      // currentTreeCardId:[]
    }
  },
  actions: {
    set(item: any) {
      const a = item
      this.bread.push(a)
    },
    setRecord(item: any) {
      this.historyRecord.push(item)
    },
    setTreeState(item) {
      this.userCardState = item
    }

  },

  persist: {
    enabled: true,
  },
})
