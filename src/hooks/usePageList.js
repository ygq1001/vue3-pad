// const { ref, unref } = require("vue")
import { ref, unref } from 'vue'
const usePageList = (listApi, baseQuery = {}) => {
    const loading = ref(false)
    const finish = ref(false)
    const list = ref([])
    const page = ref({
        page: 1,
        pageSize: 15
    })

    const resetLoad = () => {
        page.value.page = 1;
        finish.value = false
        loading.value = false
        loadData()
    }

    const loadData = async () => {
        console.log("参数", page.value);
        if (loading.value || finish.value) {
            return;
        }
        //当loading.value为false才会执行下面
        try {
            loading.value = true
            const res = await listApi?.({ ...unref(page), ...baseQuery });
            if (page.value.page == 1) {
                list.value = res.data.list
            } else {
                // @ts-ignore
                list.value.push(...res.data.list)
                if (res.data.totalCount == list.value.length) {
                    finish.value = true
                }

            }
            page.value.page++;


            //  判断是否第一页 list.value = data
            // 不是第一页   push数据

            //  判断数据是否加载完毕

            // 加载完毕 finish.value = true
            // 还有数据  page.page++;
        } catch (error) {
            console.error(error)
        } finally {
            console.log("放");
            loading.value = false
        }
    }

    return {
        loadData,
        loading,
        finish,
        list,
        resetLoad
    }
}

export default usePageList