import { createApp } from "vue"
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css';
import UserModal from '@/views/address/modal/UserModal.vue'
import VideoModal from '@/components/Modal/VideoModal.vue';
import MeetSetModal from '@/components/Modal/MeetSetModal.vue';


class Modal {
    static render(component, options = {}) {
        this.app = createApp(component, {
            ...options,
            onClose: () => this.destroy()
        })
        this.el = document.createElement('div')//创建一个新的 div 元素，并将其赋值给 this.el。
        options.mask && this.el.classList.add('mask')//如果 options 中有 mask 属性并且为 true，则给 this.el 添加一个 mask 类。
        this.app.mount(this.el)
        document.body.appendChild(this.el)
    }

    static destroy() {
        this.app?.unmount()
        this.el?.remove()
    }
}

class UserModel extends Modal {
    static render(options = {}) {
        Modal.render(UserModal, options);
    }
}

class VideoModalClass extends Modal {
    static render(options = {}) {
        UserModel.destroy();
        Modal.render(VideoModal, options);
    }
}

class SetModalClass extends Modal {
    static render(options = {}) {
        Modal.render(MeetSetModal, options);
    }
}
export { UserModel, VideoModalClass, SetModalClass }
