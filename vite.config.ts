import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
// import { resolve } from 'path'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
  ],
  resolve: {
    alias: {
      // '@': fileURLToPath(new URL('./src', import.meta.url)),
      '@': path.resolve(__dirname, "./src")
    }
  },
  build: {
    chunkSizeWarningLimit: 1500,
    rollupOptions: {
      output: {
        manualChunks(id) {
          if (id.includes('node_modules')) {
            return id.toString().split('node_modules/')[1].split('/')[0].toString();
          }
        }
      }
    }
  },
  server: {
    proxy: {
      '/authenm': {
        target: 'http://192.168.10.60:8099',
        // target: 'https://43.143.123.115:8000',

        changeOrigin: true,//是否代理跨域
        // rewrite: (path:any) => path.replace(/^\/authenm/, ""), 
      },

      '/dispatch': {
        target: 'http://192.168.10.60:8099',
        // target: 'https://43.143.123.115:8000',
        changeOrigin: true,//是否代理跨域
        // rewrite: (path:any) => path.replace(/^\/authenm/, ""),  
      },



    }
  },
})
